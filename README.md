# turbo-enigma-estacionamiento

El software para la gestión y control de un estacionamiento público debe cumplir con los siguientes módulos y aspectos generales, mismos que deberán estar programados en su totalidad en C.

## Características
1. Contar con una interfaz amigable y sencilla permitiendo el control de tiempos de entrada y salida de vehículos, cobro por minutos y mensualidades, incluir los módulos de: convenios, facturación, cálculo del costo total del tiempo de permanencia de cada vehículo, manejo de usuarios dos niveles (Administrador, Usuario), totalización de resultados al final del día, entre otros.
2. Facturación mensual, semanal y diaria.
3. Control de espacios para ingreso de diferentes tipos de placas (motos, vehículos, camiones). Este modulo debe indicar si existe espacio disponible o si el estacionamiento ya esta lleno.
4. Gestión y control de capacidad de vehículos que ingresan al estacionamiento.
5. Gestión y control de cortesías, según convenios.
6. Gestión y control diario de la Caja.
7. El control de los vehículos será mediante el número de placa y el espacio asignado del estacionamiento. Una vez ingresado el auto se activara el cronometro, él cual contabilizara el cobro de minutos según el valor establecido por el propietario del estacionamiento. Así mismo podrá generar tickets o documentos de pago (facturas) para entregar al usuario.
8. Es importante destacar que con solo digitar la placa, el administrador podrá verificar el tiempo que lleva el   vehículo en el establecimiento, si el vehículo se encuentra o no registrado, además de conocer la ubicación donde se encuentra estacionado el vehículo.
9. El numero de placa podrá ingresarse de manera ágil (validación) y segura al digitar el número de placa.
10. Cuando el vehículo salga del establecimiento, el software indicara el valor a pagar previo ingreso de la placa. Es importante destacar que previamente se debió configurar el software con el fin de señalar el valor del minuto, hora, semana o mensualidad que se desee cobrar.
11. Al imprimir el ticket de pago este deberá contener: Encabezado con datos del  establecimiento,  placa  del vehículo, Hora de entrada, Hora de salida y valor total a pagar. Es importante que si un vehículo no es registrado inicialmente no es posible que se genere el ticket o documento de pago y por lo tanto no se le permitirá salir del estacionamiento.
12. Para el administrador del estacionamiento resulta una fuente útil y segura, puesto que se podrá generar reportes de actividades diarios o mensuales. Estos reportes son exactos puesto que en cada uno de ellos quedarán especificados datos que serán inmodificables por parte de los trabajadores o administradores temporales de los estacionamientos, es importante aclarar que previamente se debe realizar dos tipos de sesiones: la del administrador y la del trabajador o de cualquier otra forma a elección del cliente.
13. Para el dueño del estacionamiento es importante cumplir con las necesidades del cliente, con este fin, el usuario administrador podrá imprimir facturas con datos de la empresa como Nombre ó Razón Social, Régimen, Dirección, Teléfono, Horarios de atención o cualquier otro dato que el cliente considere relevante.
14. Podrá solicitar diferentes tipos de sesiones según sus necesidades, podrá configurar los parámetros de tarifas y el tiempo a cobrar y podrá incluir las tarifas de cortesías según sea el convenio, lo que permitirá un adecuado funcionamiento de su organización.
## Instalación
* Primero es clonar el repositorio.
```bash
    git clone https://ErikGoH@bitbucket.org/kerrisk/turbo-enigma-estacionamiento.git
```
* Entrar al directorio donde se clono.
```bash
    cd turbo-enigma-estacionamiento
```
* Compilarlo se requiere tener gcc.
```bash
    make all
```
* Ejecutarlo
```bash
    ./turbo-enigma-estacionamiento
```

## Uso

* Autentificación
Los usuarios por default son

| Usuario | Contraseña |
|:-------:|:----------:|
| PaPa    | password   |
| DoAr    | password   | 

* Iniciando el programa
![alt text](./Images/01_inicio.png "Pantalla principal")
* Opciones del programa
![alt text](./Images/02_login.png "Opciones programa")

                Menu de Administrador
1. Ingreso de Vehiculo
2. Egreso de Vehiculo
3. Facturación
4. Reportes de actividad
5. Control de usuarios
6. Control de clientes
7. Control de convenios
8. Mostrar Espacios
9. Mostrar HistorialIngresosEgresosVehiculos
10. Mostrar PagosEstacionamientoHistorial
11. Mostrar Usuarios
12. Mostrar Clientes
13. Mostrar PagosConveniosHistorial
14. Mostrar Convenios
0. Cerrar sesion



