#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
//Integrantes: Kristian Eduardo Alba Macias y Erik Gonzalez Hernandez
/*********************************************Estructuras*********************************************************************************/
struct clientes{//Esta es la estructura donde se guardaran los datos clientes que se registren para poder obtener un precio especial.
	int id;
	char Nombre[200];
	char Domicilio[200];
	int convenio;
	int pin;
	float TiempoAcumulado;
};
struct usuarios{//Esta es la estructura donde se guarda la información de inicio de sesión de los empleados y administradores.
	int id;
	char Nombre[200];
	char NombreDeUsuario[25];
	char Contrasena[50];
	int tipo;
};
struct convenios{//Esta el la estructura donde se guardan los diferentes tipos de tasa con la que se cobra el estacionamiento.
	int id,PagoAFinDelPeriodo;
	float TasaPorMinuto, TasaPorHora, TasaPorDia, TasaPorSemana, TasaPorMes,Anualidad, LimiteDePeriodo;
};
struct espacios{//Esta es la estructuea donde se guardan los espacios del estacionamiento y el vehiculo que esta estacionado.
	int id, TipoDeVehiculo,idcliente;
	char PlacaVehicular[10];
	struct tm Ingreso;
};
struct PagosEstacionamientoHistorial{//Esta es la estructura de las transacciones de pagos a caja (ie. Cuando sale un auto y paga su couta, Cuando liquida su cuenta total acumulada).
	int id, idcliente;
	float TiempoTotal, PagoTotal, PagoPorMinuto, PagoPorHora, PagoPorDia, PagoPorSemana, PagoPorMes;
	struct tm FechaDePago;
};
struct HistorialIngresosEgresosVehiculos{//Esta es la estructura en la que se almacenan cada que entra y sale un vehiculo

	int id, idcliente,idespacio;
	char PlacaVehicular[10];
	struct tm Ingreso,Egreso;	
};
struct PagosConveniosHistorial{//Esta es la estructura donde se almacenan los pagos relacionados a convenios.
	int id, idcliente,idconvenio;
	float PagoTotal;
	struct tm FechaDePago;	
};
struct HistorialReportes{
	int id, Periodo;
	char Nombre[100];
	struct tm FechaDeCreacion;
};
/*********************************************Prototipos de las funciones************************************************************/
int MenuAdmin();
int MenuUser();
int ComprobarArchivos();
int ComprobarArchivo(char *);
int CrearCascaron(int,int, char *);
int IniciarSesion(char *, char *);
int IngresoVehiculo();
int ComprobarCliente(int, int);
int ComprobarEspacios(char *, int *);
int ComprobarEspacio(int, char *);
int ComprobarPlaca(char *);
int RegistrarIngresoDeVehiculo(char *,int, int, int);
int EgresoVehiculo();
void MostrarEspacios();
int ComprobarVehiculo(char *);
int ProcesoDeEgreso(char *,int,int);
struct tm ObtenerTiempoDeIngreso(int);
int ComprobarTipoDeConvenio(int );
int ObtenerNumeroDeConvenio(int);
int ComprobarPagoAFinDelPeriodo(int);
int ComprobarLimiteDeTiempoAcumulado(int, float);
int RegistrarEgresoDeVehiculo(int,int,char *, struct tm,struct tm);
int PagarEgresoDeVehiculo(int,int,struct tm,struct tm, int);
int AcumularTiempoACliente(int,float);
float ObtenerTiempoAcumulado(int);
int CalcularCostosDePagoEstacionamiento(float *,float *,float *,float *,float *,float *,float,int );
int MostrarPagosEstacionamientoHistorial(int);
int MostrarHistorialIngresosEgresosVehiculos();
struct convenios ObtenerInformacionConvenio(int);
int LiberarEspacioDeEstacionamiento(int,char *);
FILE * AbrirArchivo(char *,char *);
int ControlUsuarios();
int AgregarUsuario();
int ModificarUsuario();
int EliminarUsuario();
int GuardarRegistroUsuario(struct usuarios *,int);
int MostrarUsuarios();
struct usuarios ObtenerInformacionUsuario(int);
int ControlClientes();
int AgregarCliente();
int ModificarCliente();
int EliminarCliente();
int GuardarRegistroCliente(struct clientes *,int);
int MostrarClientes();
struct clientes ObtenerInformacionCliente(int);
int PagarAnualidadConvenio(int,int);
int ComprobarPagoDeAnualidad(int);
struct PagosConveniosHistorial ObtenerInformacionUltimoPagoConvenio(int);
int MostrarPagosConveniosHistorial();
int ControlConvenios();
int AgregarConvenio();
int ModificarConvenio();
int EliminarConvenio();
int GuardarRegistroConvenio(struct convenios *,int);
int MostrarConvenios();
int ConvertirSegundos(float, char *);
int Reportes();
int GenerarReporte();
int AbrirReporte();
int CrearReporteEstacionamiento(int, char*,struct tm);
int CrearReporteConvenios(int, char*,struct tm);
int MostrarHistorialReportes();
int ObtenerValoresParaPeriodoReporte(float *,char *);
void QuitarEspacios(char *);
struct HistorialReportes ObtenerInformacionReporte(int id);
int DistinguirReportes(char *);
void QuitarSaltoDeLinea(char *);
int Facturacion();
int GenerarFactura(int, int);
struct espacios ObtenerInformacionEspacio(int);
/**************************************************Inicio del Programa**********************************************************/
int main(int argc, char const *argv[]){
	/* Esta es la primera parte del programa en el cual se le pide a los usuarios que ingresen su nombre de usuario y contraseña, cuando logran iniciar sesión 
	los manda al menu que corresponde con su tipo de usuario*/
	int respuesta,continuar=1;
	char NombreDeUsuario[25],Contrasena[50];
	ComprobarArchivos();//Se manda a llamar a "ComprobarArchivos" para asegurar que los archivos necesarios existan.
	while (continuar){
		printf("\t\tIniciar sesion\n");
		printf("Para salir ingresa \'0\' en ambos campos...\n");
		printf("Ingresa tu nombre de usuario: ");
		scanf("%s",NombreDeUsuario);
		printf("Ingresa tu contrasena: ");
		scanf("%s",Contrasena);
		respuesta=IniciarSesion(NombreDeUsuario,Contrasena);
		switch(respuesta){//Dependiendo del usuario que sea es a donde se le va a redirigir.
			case 1:
				MenuAdmin();
				break;
			case -1:
				MenuUser();
				break;
			case 0:
				break;
			case -2:
				continuar=0;
				break;
		}
	}
	return 0;
}
int ComprobarArchivos(){
	//Primero comprueba el archivo Clientes.bin
	if (ComprobarArchivo("Clientes.bin")){//En caso de no existir se crea
		CrearCascaron(100,0,"Clientes.bin");//Y crea el cascaron correspondiente
	}
	//Comprueba el archivo Usuarios.bin
	if (ComprobarArchivo("Usuarios.bin")){//En caso de no existir se crea
		CrearCascaron(25,1,"Usuarios.bin");//Y crea el cascaron correspondiente
		FILE * Archivo=AbrirArchivo("Usuarios.bin","rb+");
		//Se crean 2 usuarios para poder utilizar el sistema
		struct usuarios Admin={1,"Pancho Pantera","PaPa","password",1},Empleado={2,"Doroteo Arango","DoAr","password",-1};
		fseek(Archivo,0,SEEK_SET);
		fwrite(&Admin,sizeof(struct usuarios),1,Archivo);
		fwrite(&Empleado,sizeof(struct usuarios),1,Archivo);
		fclose(Archivo);
	}
	//Comprueba el archivo Convenios.bin
	if (ComprobarArchivo("Convenios.bin")){//En caso de no existir se crea
		CrearCascaron(100,2,"Convenios.bin");//Y crea el cascaron correspondiente
		FILE * Archivo=AbrirArchivo("Convenios.bin","rb+");
		//Se inserta la tasa estandar, la que tienen los clientes que aún no se registran o no tienen convenio especial
		struct convenios Normal={1,0,.25,14,300,2000,7000,0,0};
		fseek(Archivo,0,SEEK_SET);
		fwrite(&Normal,sizeof(struct convenios),1,Archivo);
		fclose(Archivo);
	}
	//Comprueba el archivo Espacios.bin
	if (ComprobarArchivo("Espacios.bin")){//En caso de no existir se crea
		CrearCascaron(300,3,"Espacios.bin");//Y crea el cascaron correspondiente
	}
	//Comprueba el archivo PagosEstacionamientoHistorial.bin
	if (ComprobarArchivo("PagosEstacionamientoHistorial.bin")){//En caso de no existir se crea
		CrearCascaron(10000,4,"PagosEstacionamientoHistorial.bin");//Y crea el cascaron correspondiente
	}
	//Comprueba el archivo PagosConveniosHistorial.bin
	if (ComprobarArchivo("PagosConveniosHistorial.bin")){//En caso de no existir se crea
		CrearCascaron(5000,5,"PagosConveniosHistorial.bin");//Y crea el cascaron correspondiente
	}
	//Comprueba el archivo HistorialIngresosEgresosVehiculos.bin
	if (ComprobarArchivo("HistorialIngresosEgresosVehiculos.bin")){//En caso de no existir se crea
		CrearCascaron(5000,6,"HistorialIngresosEgresosVehiculos.bin");//Y crea el cascaron correspondiente
	}
	if (ComprobarArchivo("HistorialReportes.bin"))
	{
		CrearCascaron(1000,7,"HistorialReportes.bin");
	}
	return 0;
}
int ComprobarArchivo(char *nombre){//Esta función intenta el archivo que recibe en el puntero nombre y si no está, el archivo se crea.
	FILE * Archivo;
	//printf("Comprobando Archivo %s\n",nombre );
	Archivo=fopen(nombre,"rb");
	if (Archivo!=NULL){
		fclose(Archivo);
	}else {
		Archivo=AbrirArchivo(nombre,"wb");
		fclose(Archivo);
		return 1;
	}
	return 0;
}
int CrearCascaron(int n, int opcion, char *nombre){/*Esta función recibe la cantidad de registros que se debe incertar, el tipo de estructura que debe de ser y
	el nombre del archivo donde se deben guardar*/
	FILE * Archivo=AbrirArchivo(nombre,"rb+");
	struct tm TiempoCero={0,0,0,0,0,0,0,0,0};
	if (opcion==0){
		struct clientes Vacio={0,"","",0,0,0};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct clientes),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if (opcion==1){
		struct usuarios Vacio={0,"","","",0};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct usuarios),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if (opcion==2){
		struct convenios Vacio={0,0,0,0,0,0,0,0,0};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct convenios),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if (opcion==3){/*Al crear el cascaron de "Espacios.bin" se hace con 3 tipos de registro donde se le asigna el tipo de automovil para el que es el espacio
		Tipo 1 es para motocicletas, Tipo 2 es para automoviles particulares y Tipo 3 es para Camiones*/
		struct espacios VacioTipo1={0,1,0,"",TiempoCero},VacioTipo2={0,2,0,"",TiempoCero},VacioTipo3={0,3,0,"",TiempoCero};
		fseek(Archivo,0,SEEK_SET);
		for (int i = 0; i < n; ++i)//Y la escribe "n" veces en el archivo
		{
			if (i<50)
				fwrite(&VacioTipo1,sizeof(struct espacios),1,Archivo);
			else if (i<290)
				fwrite(&VacioTipo2,sizeof(struct espacios),1,Archivo);
			else
				fwrite(&VacioTipo3,sizeof(struct espacios),1,Archivo);
		}
	}
	if (opcion==4)
	{
		struct PagosEstacionamientoHistorial Vacio={0,0,0,0,0,0,0,0,0,TiempoCero};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if (opcion==5)
	{
		struct PagosConveniosHistorial Vacio={0,0,0,0,TiempoCero};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct PagosConveniosHistorial),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if (opcion==6)
	{
		struct HistorialIngresosEgresosVehiculos Vacio={0,0,0,"",TiempoCero,TiempoCero};//Se crea una estructura vacia para poder desplazarnos por el archivo
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);//Y la escribe "n" veces en el archivo
	}
	if(opcion==7){
		struct HistorialReportes Vacio={0,0,"",TiempoCero};
		for (int i = 0; i < n; ++i)
			fwrite(&Vacio,sizeof(struct HistorialReportes),1,Archivo);
	}
	if(opcion==8){
	}
	fclose(Archivo);
	return 0;
}
int IniciarSesion(char *NombreDeUsuario, char *Contrasena){
	if (*NombreDeUsuario=='0'&&*Contrasena=='0')//Primero comprueba sí el usuario quiere cerrar el programa
	{
		return -2;
	}
	FILE * Archivo=AbrirArchivo("Usuarios.bin","rb");//Sí el usuario quiere iniciar sesión abre el archivo "Usuarios.bin" para poder comprobar los datos que nos dio para iniciar sesión
	struct usuarios Temporal;//Crea un estructura para poder comprobar los parametros
	fseek(Archivo,0,SEEK_SET);
	for (int i = 0; i < 25; ++i)
	{
		fread(&Temporal,sizeof(struct usuarios),1,Archivo);//Lee un registro del archivo
		if (!strcmp(Temporal.NombreDeUsuario,NombreDeUsuario))//Compara si los usuario son iguales
		{
			if (!strcmp(Temporal.Contrasena,Contrasena))//Compara si las contrasenas son iguales
			{
				fclose(Archivo);
				return Temporal.tipo;//Regresa el tipo de usuario que es
			}else{
				fclose(Archivo);
				printf("Contrasena incorrecta...\n");//Muestra que se equivoco de contraseña y regresa
				return 0;
			}
		}
	}
	printf("Usuario no encontrado...\n");//Muestra que se equivoco de usuario y regresa
	fclose(Archivo);
	return 0;
}
int MenuAdmin(){//Se muestran los modulos a los que puede acceder el Administrador
	int respuesta=1;
	while (respuesta){
		printf("\t\tMenu de Administrador\n");
		printf("1.-Ingreso de Vehiculo\n");//Punto 3, 4, 7, 9
		printf("2.-Egreso de Vehiculo\n");//Punto 6, 10, 11
		printf("3.-Facturación\n");//Punto 2, 13
		printf("4.-Reportes de actividad\n");//Punto 12
		printf("5.-Control de usuarios\n");//Punto 14
		printf("6.-Control de clientes\n");//Punto 14
		printf("7.-Control de convenios\n");//
		printf("8.-Mostrar Espacios\n");
		printf("9.-Mostrar HistorialIngresosEgresosVehiculos\n");
		printf("10.-Mostrar PagosEstacionamientoHistorial\n");
		printf("11.-Mostrar Usuarios\n");
		printf("12.-Mostrar Clientes\n");
		printf("13.-Mostrar PagosConveniosHistorial\n");
		printf("14.-Mostrar Convenios\n");
		printf("0.-Cerrar sesion\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				IngresoVehiculo();
				break;
			case 2:
				EgresoVehiculo();
				break;
			case 3:
				Facturacion();
				break;
			case 4:
				Reportes();
				break;
			case 5:
				ControlUsuarios();
				break;
			case 6:
				ControlClientes();
				break;
			case 7:
				ControlConvenios();
				break;
			case 8:
				MostrarEspacios();
				break;
			case 9:
				MostrarHistorialIngresosEgresosVehiculos();
				break;
			case 10:
				MostrarPagosEstacionamientoHistorial(1);
				break;
			case 11:
				MostrarUsuarios();
				break;
			case 12:
				MostrarClientes();
				break;
			case 13:
				MostrarPagosConveniosHistorial(1);
				break;
			case 14:
				MostrarConvenios();
				break;
			case 0:
				break;
			default:
				printf("Opcion no encontrada...\n");
				break;
		}
	}
	return 0;
}
int MenuUser(){//Se muestran los modulos a los que puede acceder el Empleado
	int respuesta=1;
	while (respuesta){
		printf("\t\tMenu de Usuario\n");
		printf("1.-Ingreso de Vehiculo\n");//Punto 3, 4, 7, 9
		printf("2.-Egreso de Vehiculo\n");//Punto 6, 10, 11
		printf("3.-Control de clientes\n");//Punto 14
		printf("4.-Control de convenios\n");//
		printf("0.-Cerrar sesion\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				IngresoVehiculo();
				break;
			case 2:
				EgresoVehiculo();
				break;
			case 3:
				ControlClientes();
				break;
			case 4:
				ControlConvenios();
				break;
			case 0:
				break;
			default:
				printf("Opcion no encontrada...\n");
				break;
		}
	}
	return 0;
}
int IngresoVehiculo(){
	char PlacaVehicular[10];
	int id=0,pin=0,espacio, TipoDeVehiculo;
	printf("\t\tIngreso de vehiculos\n");//Esta función te va a pedir que ingreses la placa vehicular
	printf("Ingresa la placa vehicular (sin espacios ni guiones): ");
	getchar();
	scanf("%[^\n]",PlacaVehicular);
	printf("Ingresa el id del cliente (si no se ha registrado ingresa \'0\'): ");//Pide el id del cliente
	scanf("%i",&id);
	struct clientes Temporal_Cliente=ObtenerInformacionCliente(id);
	if (id&&Temporal_Cliente.id!=id)
	{
		printf("El usuario ingresado no existe...\n");
		return 0;
	}
	if (id)
	{
		printf("Ingresa el pin: ");//Esto es para asegurar que es el cliente 
		scanf("%i",&pin);
		if(!ComprobarCliente(id, pin)){//Si el pin no coincide no permite hacer continuar con el registro
			printf("El pin ingresado es incorrecto\n");
			return 0;
		}
	}
	espacio=ComprobarEspacios(PlacaVehicular,&TipoDeVehiculo);//Se obtiene el numero de espacio que le va a tocar al cliente
	printf("El espacio que le toca es: %i\n",espacio);
	if (espacio>0)
	{
		printf("Espacio disponible. Adelante...\n");
		RegistrarIngresoDeVehiculo(PlacaVehicular, id,espacio, TipoDeVehiculo);//Procede al registro del vehiculo en el espacio que le corresponde
	}else if (espacio == -1)
	{
		printf("No hay espacio disponible para este vehiculo...\n");
	}else if (espacio==-2)
	{
		printf("Esta placa vehicular ya ha sido registrada...\n");
	}
	return 0;
}
int ComprobarCliente(int id, int pin){//Esta función simplemente checa que el id y el pin dado por el cliente sean correctos
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb");
	struct clientes Temporal;
	fseek(Archivo,(id-1)*sizeof(struct clientes),SEEK_SET);
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	if (Temporal.pin==pin)
	{
		fclose(Archivo);
		return 1;
	}
	fclose(Archivo);
	return 0;
}
int ComprobarEspacios(char *PlacaVehicular, int *TipoDeVehiculo){//Esta función comprueba la placa vehicular y obtiene el espacio en que se puede estacionar
	*TipoDeVehiculo=ComprobarPlaca(PlacaVehicular);
	//printf("El vehiculo es de tipo: %i\n",*TipoDeVehiculo);
	return ComprobarEspacio(*TipoDeVehiculo, PlacaVehicular);
}
int ComprobarPlaca(char *PlacaVehicular){
	/*Cada tipo de vehiculo cuenta con un patron de letras y numeros diferentes
	Placas de vehiculos:
	Tipo 1: Motocicleta: Son 5 digitos alfanumericos
	Tipo 2: Carros particulares: tres letras seguidas de cuatro o tres números.
	Tipo 3: Camiones: dos letras seguidas de cinco o cuatro números
	Esta función checa cual tipo de vehiculo es y lo regresa*/
	int cont=0,letras=0,numeros=0;
	for (int i = 0; PlacaVehicular[i]!='\0'; ++i)
	{
		cont++;
		if (isdigit(PlacaVehicular[i]))
		{
			numeros++;
		}else if(isalpha(PlacaVehicular[i])){
			letras++;
		}
	}
	//printf("Comprobando Placa Vehicular...\nLetras: %i\tNumeros: %i\tTotal: %i\n",letras,numeros,cont);
	if (cont>=5&&(letras+numeros)==5)
	{
		return 1;
	}else if (cont>=6&&letras==3&&(numeros==3||numeros==4))
	{
		return 2;
	}else if (cont>=6&&letras==2&&(numeros==5||numeros==4))
	{
		return 3;
	}
	return 0;
}
int ComprobarEspacio(int TipoDeVehiculo, char *PlacaVehicular){
	FILE * Archivo=AbrirArchivo("Espacios.bin","rb");
	fseek(Archivo,0,SEEK_SET);
	struct espacios Temporal;
	for (int i = 0; i < 300; ++i){
		fread(&Temporal,sizeof(struct espacios),1,Archivo);
		if (Temporal.id&&!strcmp(Temporal.PlacaVehicular,PlacaVehicular))//Comprueba con todos los espacios que tenga un carro registrado que no se repitan las mismas placas vehiculares
		{
			//printf("Caso -2\n");
			fclose(Archivo);
			return -2;
		}
		if (Temporal.id == 0 && Temporal.TipoDeVehiculo==TipoDeVehiculo){//Busca un espacio vacio y del mismo tipo que el vehiculo
			//printf("Caso 1\n");
			fclose(Archivo);
			return ++i;
		}
	}
	//printf("Caso -1\n");
	fclose(Archivo);
	return -1;
}
int RegistrarIngresoDeVehiculo(char *PlacaVehicular, int idcliente,  int espacio, int TipoDeVehiculo){
	/*Esta función simplemente modifica el registro correspondiente al espacio el cual se le asigno al cliente*/
	FILE *Archivo=AbrirArchivo("Espacios.bin","rb+");
	time_t raw_time;
	struct tm Ingreso;
	time(&raw_time);
	Ingreso=*localtime(&raw_time);
	struct espacios NuevoRegistroEspacio={espacio,TipoDeVehiculo,idcliente,"",Ingreso};
	strcpy(NuevoRegistroEspacio.PlacaVehicular,PlacaVehicular);
	fseek(Archivo,(espacio-1)*sizeof(struct espacios),SEEK_SET);
	fwrite(&NuevoRegistroEspacio,sizeof(struct espacios),1,Archivo);
	printf("Hora de ingreso: %s\n",asctime(&Ingreso));
	fclose(Archivo);
	return 0;
}
void MostrarEspacios(){//Esta función puede mostrar los espacios ocupados o libres
	FILE * Archivo=AbrirArchivo("Espacios.bin","rb");
	printf("\t\tEspacios\n");
	printf("Selecciona: Espacios ocupados = 1\tEspacios Libres = 0\n");
	int n;
	scanf("%i",&n);
	struct espacios Temporal;
	for (int i = 0; i < 300; ++i)
	{
		fseek(Archivo,i*sizeof(struct espacios),SEEK_SET);
		fread(&Temporal,sizeof(struct espacios),1,Archivo);
		if (n?Temporal.id:!Temporal.id)
		{
			printf("%i | %i | %i | %s | %s\n",Temporal.id,Temporal.TipoDeVehiculo,Temporal.idcliente,Temporal.PlacaVehicular,asctime(&Temporal.Ingreso));
		}
	}
	return;
}
int EgresoVehiculo(){
	char PlacaVehicular[10];
	int id=0,pin=0,espacio;
	printf("\t\tEgreso de vehiculos\n");//Esta función te va a pedir que ingreses la placa vehicular
	printf("Ingresa \'0\' en ambos campos para salir\n");
	printf("Ingresa la placa vehicular (sin espacios ni guiones): ");
	getchar();
	scanf("%[^\n]",PlacaVehicular);
	printf("Ingresa el id del cliente (si no se ha registrado ingresa \'0\'): ");//Pide el id del cliente
	scanf("%i",&id);
	if (!id&&!strcmp(PlacaVehicular,"0"))
	{
		printf("Saliendo...\n");
		return 0;
	}
	struct clientes Temporal_Cliente=ObtenerInformacionCliente(id);
	if (id&&Temporal_Cliente.id!=id)
	{
		printf("El usuario ingresado no existe...\n");
		return 0;
	}
	espacio=ComprobarVehiculo(PlacaVehicular);
	struct espacios Temporal_espacio=ObtenerInformacionEspacio(espacio);
	if (Temporal_espacio.idcliente != id)
	{
		printf("El id de usuario no coincide con el que se registro...\nSe han informado a las autoridaes correspondientes\n");
		return 0;
	}
	if (id)
	{
		printf("Ingresa el pin: ");//Esto es para asegurar que es el cliente 
		scanf("%i",&pin);
		if(!ComprobarCliente(id, pin)){//Si el pin no coincide no permite hacer continuar con el egreso
			printf("El pin ingresado es incorrecto\n");
			return 0;
		}
	}
	if (espacio)
	{
		printf("El automovil se encuentra listo para salir...\n");
		ProcesoDeEgreso(PlacaVehicular,id,espacio);
	}else {
		printf("El vehiculo no esta resgistrado en el sistema, se ha informado a las autoridades correspondientes...\n");
	}
	return 0;
}
int ComprobarVehiculo(char *PlacaVehicular){
	FILE * Archivo=AbrirArchivo("Espacios.bin","rb");
	fseek(Archivo,0,SEEK_SET);
	struct espacios Temporal;
	for (int i = 0; i < 300; ++i){
		fread(&Temporal,sizeof(struct espacios),1,Archivo);
		if (Temporal.id&&!strcmp(Temporal.PlacaVehicular,PlacaVehicular))//Comprueba con todos los espacios que tenga un carro registrado para saber si el carro fue registrado
		{
			fclose(Archivo);
			return Temporal.id;
		}
	}
	fclose(Archivo);
	return 0;//Si no se encontro regresa 0
}
int ProcesoDeEgreso(char *PlacaVehicular,int id,int espacio){//Inicia el proceso para dejar salir a un vehiculo
	int PagoAFinDelPeriodo,Pagar=-1,DentroDelLimite;
	struct tm Egreso,Ingreso=ObtenerTiempoDeIngreso(espacio);
	time_t raw_time;
	time(&raw_time);
	Egreso=*localtime(&raw_time);
	if (id)//Si se ingreso un id de cliente se comprueba si tiene un convenio para poder pagar a fin del periodo.
	{
		PagoAFinDelPeriodo=ComprobarTipoDeConvenio(id);
		if (PagoAFinDelPeriodo)
		{
			DentroDelLimite=ComprobarLimiteDeTiempoAcumulado(id,difftime(mktime(&Egreso),mktime(&Ingreso)));//Comprueba si el cliente a un puede seguir almacenanado el tiempo o ya lo tiene que liquidar
			if (DentroDelLimite)
			{
				printf("Deseas pagar o acumularlo a tu cuenta? (1=pagar y 0=Acumular)\n");
				scanf("%i",&Pagar);
			}else{
				printf("Has superado el limite de tu convenio, lo tienes que liquidar para poder continuar...\n");
				Pagar=-1;
			}
		}
		if(!ComprobarPagoDeAnualidad(id)){
			PagarAnualidadConvenio(ObtenerNumeroDeConvenio(id),id);
		}
	}
	if (Pagar==1)
	{
		RegistrarEgresoDeVehiculo(id,espacio,PlacaVehicular,Ingreso,Egreso);
		PagarEgresoDeVehiculo(id,espacio,Ingreso,Egreso,Pagar);
	}else if (Pagar==0){
		RegistrarEgresoDeVehiculo(id,espacio,PlacaVehicular,Ingreso,Egreso);
		AcumularTiempoACliente(id,difftime(mktime(&Egreso),mktime(&Ingreso)));
	}else if (Pagar==-1)
	{
		RegistrarEgresoDeVehiculo(id,espacio,PlacaVehicular,Ingreso,Egreso);
		PagarEgresoDeVehiculo(id,espacio,Ingreso,Egreso,Pagar);
	}
	return 0;
}
struct espacios ObtenerInformacionEspacio(int idespacio){
	FILE *Archivo=AbrirArchivo("Espacios.bin","rb");
	struct espacios Temporal;
	fseek(Archivo,(idespacio-1)*sizeof(struct espacios),SEEK_SET);
	fread(&Temporal,sizeof(struct espacios),1,Archivo);
	fclose(Archivo);
	return	Temporal;
}
int ComprobarLimiteDeTiempoAcumulado(int idcliente, float Tiempo){//Comprueba si a un puede seguir almacenanado el tiempo o ya lo tiene que liquidar
	struct clientes TemporalCliente=ObtenerInformacionCliente(idcliente);
	struct convenios TemporalConvenio=ObtenerInformacionConvenio(TemporalCliente.convenio);
	if ((TemporalCliente.TiempoAcumulado+Tiempo)>=TemporalConvenio.LimiteDePeriodo)
		return 0;
	return 1;
}
int ComprobarPagoDeAnualidad(int id){
	struct PagosConveniosHistorial Temporal=ObtenerInformacionUltimoPagoConvenio(id);
	//printf("%i %i %i %f %s\n",Temporal.id,Temporal.idcliente,Temporal.idconvenio,Temporal.PagoTotal,asctime(&Temporal.FechaDePago));
	time_t raw_time;//Se crea una variable que almacena el tiempo
	time(&raw_time);//Se le almacena el tiempo
	struct tm TiempoActual=*localtime(&raw_time);//La función localtime lo convierte a nuestra zona horaria y en forma de estructura
	//printf("Tiempo Actual: %s",asctime(&TiempoActual));
	//printf("FechaDePago: %s",asctime(&Temporal.FechaDePago));
	float TiempoDesdeUltimoPago=difftime(mktime(&TiempoActual),mktime(&Temporal.FechaDePago));
	//printf("TiempoDesdeUltimoPago=%f\n",TiempoDesdeUltimoPago );
	//printf("TiempoDesdeUltimoPago>=31536000=%i\n",TiempoDesdeUltimoPago>=31536000);
	//printf("El ultimo pago de anualidad fue hace %f segundos \n",TiempoDesdeUltimoPago);
	if (TiempoDesdeUltimoPago>=31536000)
		return 0;
	return 1;
}
struct tm ObtenerTiempoDeIngreso(int espacio){//Esta función recibe un espacio de estacionamiento y regresa la hora en la que ingreso
	FILE *Archivo=AbrirArchivo("Espacios.bin","rb");
	struct espacios Temporal;
	fseek(Archivo,(espacio-1)*sizeof(struct espacios),SEEK_SET);
	fread(&Temporal,sizeof(struct espacios),1,Archivo);
	fclose(Archivo);
	return Temporal.Ingreso;
}
int ComprobarTipoDeConvenio(int id){//Con un id de cliente obtiene el numero de convenio que tiene y si tiene la opción de hacer sus pagos a fin de periodo.
	int idconvenio;
	idconvenio=ObtenerNumeroDeConvenio(id);
	return ComprobarPagoAFinDelPeriodo(idconvenio);
}
int ObtenerNumeroDeConvenio(int id){//Con el id de cliente, se desplaza a la posición correspondiente y regresa el id de convenio que tiene.
	if (!id){
		return 1;
	}
	FILE * Archivo=AbrirArchivo("Clientes.bin","rb");
	struct clientes Temporal;
	fseek(Archivo,(id-1)*sizeof(struct clientes),SEEK_SET);
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	fclose(Archivo);
	return Temporal.convenio;
}
int ComprobarPagoAFinDelPeriodo(int id){//Con el id de convenio, se desplaza a la posición correspondiente y regresa el valor de la variable "PagoAFinDelPeriodo".
	FILE * Archivo=AbrirArchivo("Convenios.bin","rb");
	struct convenios Temporal;
	fseek(Archivo,(id-1)*sizeof(struct convenios),SEEK_SET);
	fread(&Temporal,sizeof(struct convenios),1,Archivo);
	fclose(Archivo);
	return Temporal.PagoAFinDelPeriodo;
}
int RegistrarEgresoDeVehiculo(int idcliente, int idespacio,char *PlacaVehicular, struct tm Ingreso,struct tm Egreso){
	//Esta función registra cuando un vehiculo sale del estacionamiento
	FILE *Archivo=AbrirArchivo("HistorialIngresosEgresosVehiculos.bin","rb+");
	int i=0;
	struct HistorialIngresosEgresosVehiculos Temporal;
	printf("Registrando egreso de vehiculo...\n");
	fread(&Temporal,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);
	while(!feof(Archivo)){//Recorre el archivo hasta el final
		++i;
		if (!Temporal.id)//Encuetra el primer registro vacio y ahí es donde lo guarda
		{
			Temporal.id=i;
			Temporal.idcliente=idcliente;
			Temporal.idespacio=idespacio;
			strcpy(Temporal.PlacaVehicular,PlacaVehicular);
			Temporal.Ingreso=Ingreso;
			Temporal.Egreso=Egreso;
			fseek(Archivo,(i-1)*sizeof(struct HistorialIngresosEgresosVehiculos),SEEK_SET);
			fwrite(&Temporal,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);
			fclose(Archivo);
			LiberarEspacioDeEstacionamiento(idespacio, PlacaVehicular);
			return 0;
		}
		fread(&Temporal,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);
	}//Si no encuentra ninguno vacio supone que ya no hay espacio en el archivo y le pide que haga un respaldo y lo elimine para que se cree uno completamente nuevo.
	fclose(Archivo);
	printf("El archivo HistorialIngresosEgresosVehiculos.bin ha llegado a su maxima capacidad.\nHas un respaldo, elimina este y finalmente vuelve a iniciar el programa...\n");
	exit (1);
}
int PagarEgresoDeVehiculo(int idcliente,int idespacio, struct tm Ingreso, struct tm Egreso, int Pagar){
	//Esta función registra cuando se liquida un pago puede ser normal o acumulativo
	FILE *Archivo=AbrirArchivo("PagosEstacionamientoHistorial.bin","rb+");
	double TiempoTotal=0;
	int acumulado,i=0;
	if (idcliente)
	{
		if (Pagar==1)
		{
			printf("Quieres liqudar todo tu tiempo acumulado o solo esta ultima sesion? (1=Liquidar todo el tiempo|0=Solo este ultimo tiempo)\n");
			scanf("%i",&acumulado);
			if (acumulado)
			{
				TiempoTotal=ObtenerTiempoAcumulado(idcliente);
			}
		}else if (Pagar == -1)
		{
			TiempoTotal=ObtenerTiempoAcumulado(idcliente);
		}
		
	}
	struct PagosEstacionamientoHistorial Temporal;
	fread(&Temporal,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
	while(!feof(Archivo))//Recorre el archivo hasta el final
	{
		++i;
		if (!Temporal.id)//Encuetra el primer registro vacio y ahí es donde lo guarda
		{
			Temporal.id=i;
			Temporal.idcliente=idcliente;
			Temporal.FechaDePago=Egreso;
			TiempoTotal+=difftime(mktime(&Egreso),mktime(&Ingreso));
			Temporal.TiempoTotal=TiempoTotal;
			CalcularCostosDePagoEstacionamiento(&Temporal.PagoTotal, &Temporal.PagoPorMinuto, &Temporal.PagoPorHora, &Temporal.PagoPorDia, &Temporal.PagoPorSemana, &Temporal.PagoPorMes,Temporal.TiempoTotal, idcliente);
			fseek(Archivo,(i-1)*sizeof(struct PagosEstacionamientoHistorial),SEEK_SET);
			fwrite(&Temporal,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
			printf("El pago total es de %f\n",Temporal.PagoTotal);
			if (acumulado)
			{
				AcumularTiempoACliente(idcliente,-1*Temporal.TiempoTotal);
			}
			fclose(Archivo);
			return 0;
		}
		fread(&Temporal,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
	}//Si no encuentra ninguno vacio supone que ya no hay espacio en el archivo y le pide que haga un respaldo y lo elimine para que se cree uno completamente nuevo.
	printf("El archivo PagosEstacionamientoHistorial.bin ha llegado a su maxima capacidad.\nHas un respaldo, elimina este y finalmente vuelve a iniciar el programa...\n");
	exit (1);
	return 0;
}
int AcumularTiempoACliente(int idcliente,float TiempoTotal){//Esta función recibe un valor de tiempo y se lo acumula al registro del cliente
	FILE * Archivo=AbrirArchivo("Clientes.bin","rb+");
	struct clientes Temporal;
	fseek(Archivo,(idcliente-1)*sizeof(struct clientes),SEEK_SET);
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	Temporal.TiempoAcumulado+=TiempoTotal;
	fseek(Archivo,(idcliente-1)*sizeof(struct clientes),SEEK_SET);
	fwrite(&Temporal,sizeof(struct clientes),1,Archivo);
	fclose(Archivo);
	return 0;
}
float ObtenerTiempoAcumulado(int idcliente){//Esta función regresa el tiempo acumulado de un cliente
	struct clientes Temporal=ObtenerInformacionCliente(idcliente);
	return Temporal.TiempoAcumulado;
}
int CalcularCostosDePagoEstacionamiento(float *PagoTotal,float *PagoPorMinuto,float *PagoPorHora,float *PagoPorDia,float *PagoPorSemana,float *PagoPorMes,float TiempoTotal,int idcliente){
	//Esta función calcula los pagos parciales y los suma para obtener el total, guarda los valores por referencia
	int idconvenio,cociente=0;
	*PagoPorMes=0;
	*PagoPorSemana=0;
	*PagoPorDia=0;
	*PagoPorHora=0;
	*PagoPorMinuto=0;
	idconvenio=ObtenerNumeroDeConvenio(idcliente);
	struct convenios Temporal=ObtenerInformacionConvenio(idconvenio);
	while(TiempoTotal>0){
		if (TiempoTotal>=2419200 && Temporal.TasaPorMes)
		{
			cociente=TiempoTotal/2419200;
			*PagoPorMes+=cociente*Temporal.TasaPorMes;
			TiempoTotal-=cociente*2419200;
			continue;
		}
		if (TiempoTotal>=604800 && Temporal.TasaPorSemana)
		{
			cociente=TiempoTotal/604800;
			*PagoPorSemana+=cociente*Temporal.TasaPorSemana;
			TiempoTotal-=cociente*604800;
			continue;
		}
		if (TiempoTotal>=86400 && Temporal.TasaPorDia)
		{
			cociente=TiempoTotal/86400;
			*PagoPorDia+=cociente*Temporal.TasaPorDia;
			TiempoTotal-=cociente*86400;
			continue;
		}
		if (TiempoTotal>=3600 && Temporal.TasaPorHora)
		{
			cociente=TiempoTotal/3600;
			*PagoPorHora+=cociente*Temporal.TasaPorHora;
			TiempoTotal-=cociente*3600;
			continue;
		}
		if (TiempoTotal>0 && Temporal.TasaPorMinuto)
		{
			*PagoPorMinuto+=TiempoTotal*(Temporal.TasaPorMinuto/60);
			TiempoTotal-=TiempoTotal;
		}
	}
	*PagoTotal=*PagoPorMes+*PagoPorSemana+*PagoPorDia+*PagoPorHora+*PagoPorMinuto;
	return 0;
}
int MostrarPagosEstacionamientoHistorial(int Mostrar){//Muestra todos los pagos que se han hecho al liquidar el tiempo de estancia dentro del estacionamiento
	FILE *Archivo=AbrirArchivo("PagosEstacionamientoHistorial.bin","rb+");
	int contador=0;
	struct PagosEstacionamientoHistorial Temporal;
	fread(&Temporal,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			contador++;
			if (Mostrar)
			{
				printf("%i %i %f %s",Temporal.id,Temporal.idcliente,Temporal.PagoTotal,asctime(&Temporal.FechaDePago));
			}
		}
		fread(&Temporal,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
	}
	return contador;
}
int MostrarHistorialIngresosEgresosVehiculos(){//Muestra todos los ingresos y egresos que estan registrados en el archivo "HistorialIngresosEgresosVehiculos.bin"
	FILE *Archivo=AbrirArchivo("HistorialIngresosEgresosVehiculos.bin","rb+");
	struct HistorialIngresosEgresosVehiculos Temporal;
	char Ingreso[25];
	fread(&Temporal,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			strcpy(Ingreso,asctime(&Temporal.Ingreso));
			QuitarSaltoDeLinea(Ingreso);
			printf("%i %i %i %s %s",Temporal.id,Temporal.idcliente,Temporal.idespacio,Temporal.PlacaVehicular,Ingreso);
			printf(" %s", asctime(&Temporal.Egreso));
		}
		fread(&Temporal,sizeof(struct HistorialIngresosEgresosVehiculos),1,Archivo);
	}
	return 0;
}
int LiberarEspacioDeEstacionamiento(int espacio,char *PlacaVehicular){
	/*Esta función simplemente modifica el registro correspondiente al espacio el cual se le asigno al cliente y lo deja vacio*/
	FILE *Archivo=AbrirArchivo("Espacios.bin","rb+");
	int TipoDeVehiculo=ComprobarPlaca(PlacaVehicular);
	struct tm TiempoCero={0,0,0,0,0,0,0,0,0};
	struct espacios Vacio={0,TipoDeVehiculo,0,"",TiempoCero};
	fseek(Archivo,(espacio-1)*sizeof(struct espacios),SEEK_SET);
	fwrite(&Vacio,sizeof(struct espacios),1,Archivo);
	fclose(Archivo);
	return 0;
}
FILE * AbrirArchivo(char *Nombre,char *Modo){//Esta función recibe el nombre de un archivo y en que modo abrirlo y regresa el puntero FIlE si es que no hay ningun error.
	FILE *Archivo=fopen(Nombre,Modo);
	if (Archivo==NULL){
		printf("AbrirArchivo(%s,%s)\n",Nombre,Modo);
		perror("Error: ");
		exit(1);
	}
	return Archivo;
}
int ControlUsuarios(){//Muestra el menu para control de usuarios
	int respuesta=1;
	while(respuesta){
		printf("\t\tModulo de Control de Usuarios\n");
		printf("1.-Agregar usuario\n");
		printf("2.-Modificar usuario\n");
		printf("3.-Eliminar usuario\n");
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				AgregarUsuario();
				break;
			case 2:
				ModificarUsuario();
				break;
			case 3:
				EliminarUsuario();
				break;
			case 0:
				break;
			default:
				printf("Opción no encontrada...\n");
				break;
		}
	}
	return 0;
}
int AgregarUsuario(){//Esta funcion pide los datos necesarios para crear un nuevo usuario y lo guarda
	getchar();
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	fclose(Archivo);
	struct usuarios NuevoRegistroUsuario;
	NuevoRegistroUsuario.id=0;
	printf("\t\tModulo de Control de Usuarios\n");
	printf("\t\t\tAgregar Usuario\n");
	printf("Ingresa \'0\' en todos los campos para cancelar\n");
	printf("Ingresa el nombre completo del usuario: ");
	scanf("%[^\n]",NuevoRegistroUsuario.Nombre);
	printf("Ingresa el nombre de usuario (para iniciar sesión en el sistema): ");
	scanf("%s",NuevoRegistroUsuario.NombreDeUsuario);
	printf("Ingresa la contraseña del usuario: ");
	scanf("%s",NuevoRegistroUsuario.Contrasena);
	printf("Ingresa el tipo de usuario (1=Administrador|-1=Empleado): ");
	scanf("%i",&NuevoRegistroUsuario.tipo);
	if (!strcmp(NuevoRegistroUsuario.Nombre,"0")&&!strcmp(NuevoRegistroUsuario.Nombre,"0")&&!strcmp(NuevoRegistroUsuario.Nombre,"0")&&!NuevoRegistroUsuario.tipo)
	{
		printf("Registro de usuario cancelado...\n");
		return -1;
	}
	GuardarRegistroUsuario(&NuevoRegistroUsuario,0);
	return 0;
}
int ModificarUsuario(){//Esta función se encarga de obtener los datos de un usuario y permitirle cambiarlos o no.
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	fclose(Archivo);
	struct usuarios Temporal;
	int id=0,respuesta=1;
	printf("\t\tModulo de Control de Usuarios\n");
	printf("\t\t\tModificar Usuario\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del usuario: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionUsuario(id);//Recupera la información que tiene el usuario
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El usuario no existe\n");
		return -1;
	}
	while (respuesta){
		printf("\t\tModulo de Control de Usuarios\n");
		printf("\t\tModificar Usuario\n");
		printf("1.-Nombre completo del usuario: \"%s\"\n",Temporal.Nombre);
		printf("2.-Nombre de usuario (para iniciar sesión en el sistema): \"%s\"\n",Temporal.NombreDeUsuario);
		printf("3.-Contraseña del usuario: \"%s\"\n",Temporal.Contrasena);
		printf("4.-El tipo de usuario (1=Administrador|-1=Empleado): \"%i\"\n",Temporal.tipo);
		printf("0.-Guardar y salir\n");
		printf("-1.-Salir sin guardar\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				getchar();
				printf("Ingresa el nuevo Nombre: ");
				scanf("%[^\n]",Temporal.Nombre);
				break;
			case 2:
				printf("Ingresa el nuevo nombre de usuario (para iniciar sesión en el sistema): ");
				scanf("%s",Temporal.NombreDeUsuario);
				break;
			case 3:
				printf("Ingresa la nueva contraseña del usuario: ");
				scanf("%s",Temporal.Contrasena);
				break;
			case 4:
				printf("Ingresa el nuevo tipo de usuario (1=Administrador|-1=Empleado): ");
				scanf("%i",&Temporal.tipo);
				break;
			case 0:
				GuardarRegistroUsuario(&Temporal,Temporal.id);
				break;
			case -1:
				respuesta = 0;
				break;
		}
	}
	return 0;
}
int GuardarRegistroUsuario(struct usuarios *RegistroUsuario, int idusuario){//Esta función sirve para guardar un registro de usuario totalmente nuevo, modificar alguno o eliminarlo
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	struct usuarios Temporal;
	int i=0;
	fread(&Temporal,sizeof(struct usuarios),1,Archivo);
	if (!idusuario&&(*RegistroUsuario).id)
	{
		fseek(Archivo,((*RegistroUsuario).id-1)*sizeof(struct usuarios),SEEK_SET);
		(*RegistroUsuario).id=0;
		strcpy((*RegistroUsuario).Nombre,"");
		strcpy((*RegistroUsuario).NombreDeUsuario,"");
		strcpy((*RegistroUsuario).Contrasena,"");
		(*RegistroUsuario).tipo=0;
		fwrite(RegistroUsuario,sizeof(struct usuarios),1,Archivo);
	}else if(!idusuario&&!(*RegistroUsuario).id){
		while(!feof(Archivo)){
			++i;
			//printf("%i | %i |%i\n",i,Temporal.id ,idusuario);
			if (!Temporal.id)
			{
				(*RegistroUsuario).id=i;
				fseek(Archivo,((*RegistroUsuario).id-1)*sizeof(struct usuarios),SEEK_SET);
				fwrite(RegistroUsuario,sizeof(struct usuarios),1,Archivo);
				fseek(Archivo,0,SEEK_END);
			}
			fread(&Temporal,sizeof(struct usuarios),1,Archivo);
		}
		if (!(*RegistroUsuario).id)
			printf("Ya no hay más espacio para usuarios...\n");
	}else if (idusuario&&(*RegistroUsuario).id)
	{
		fseek(Archivo,((*RegistroUsuario).id-1)*sizeof(struct usuarios),SEEK_SET);
		fwrite(RegistroUsuario,sizeof(struct usuarios),1,Archivo);
	}
	fclose(Archivo);
	return 0;
}
struct usuarios ObtenerInformacionUsuario(int id){//Regresa la información de un usuario
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	struct usuarios Temporal;
	fseek(Archivo,(id-1)*sizeof(struct usuarios),SEEK_SET);
	fread(&Temporal,sizeof(struct usuarios),1,Archivo);
	fclose(Archivo);
	return Temporal;
}
int EliminarUsuario(){//Elimina a un usuario por medio de su id
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	fclose(Archivo);
	struct usuarios Temporal;
	int id=0,respuesta;
	printf("\t\tModulo de Control de Usuarios\n");
	printf("\t\t\tEliminar Usuario\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del usuario: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionUsuario(id);//Recupera la información que tiene el usuario
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El usuario no existe\n");
		return -1;
	}
	printf("¿Realmente deseas eliminar al pobre \"%s\"? (1=Si|0=No)\n",Temporal.Nombre);
	printf("%i %s %s %i\n",Temporal.id,Temporal.Nombre,Temporal.NombreDeUsuario,Temporal.tipo);
	scanf("%i",&respuesta);
	if (respuesta)
		GuardarRegistroUsuario(&Temporal,0);
	else
		printf("\"%s\" aun tiene empleo...\n",Temporal.Nombre);
	return 0;
}
int MostrarUsuarios(){//Muestra todos los usuarios registrados
	FILE *Archivo=AbrirArchivo("Usuarios.bin","rb+");
	struct usuarios Temporal;
	fread(&Temporal,sizeof(struct usuarios),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			printf("%i %s %s %i\n",Temporal.id,Temporal.Nombre,Temporal.NombreDeUsuario,Temporal.tipo);
		}
		fread(&Temporal,sizeof(struct usuarios),1,Archivo);
	}
	return 0;
}
int ControlClientes(){//Muestra el menu para control de clientes
	int respuesta=1;
	while(respuesta){
		printf("\t\tModulo de Control de Clientes\n");
		printf("1.-Agregar cliente\n");
		printf("2.-Modificar cliente\n");
		printf("3.-Eliminar cliente\n");
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				AgregarCliente();
				break;
			case 2:
				ModificarCliente();
				break;
			case 3:
				EliminarCliente();
				break;
			case 0:
				break;
			default:
				printf("Opción no encontrada...\n");
				break;
		}
	}
	return 0;
}
int AgregarCliente(){//Esta funcion pide los datos necesarios para crear un nuevo cliente y lo guarda
	getchar();
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	fclose(Archivo);
	struct clientes NuevoRegistroClientes;
	NuevoRegistroClientes.id=0;
	printf("\t\tModulo de Control de Clientes\n");
	printf("\t\t\tAgregar Clientes\n");
	printf("Ingresa \'0\' en todos los campos para cancelar\n");
	printf("Ingresa el nombre completo del cliente: ");
	scanf("%[^\n]",NuevoRegistroClientes.Nombre);
	getchar();
	printf("Ingresa el domicilio del cliente: ");
	scanf("%[^\n]",NuevoRegistroClientes.Domicilio);
	do{
		printf("Ingresa el convenio del cliente (1 es la tasa estandar): ");
		scanf("%i",&NuevoRegistroClientes.convenio);
	}while(NuevoRegistroClientes.convenio<0||NuevoRegistroClientes.convenio>100);
	printf("Ingresa el pin del cliente (este sirve para autenticar tu identidad cuando entres al estacionamiento): ");
	scanf("%i",&NuevoRegistroClientes.pin);
	if (!strcmp(NuevoRegistroClientes.Nombre,"0")&&!strcmp(NuevoRegistroClientes.Domicilio,"0")&&!NuevoRegistroClientes.pin&&!NuevoRegistroClientes.convenio)
	{
		printf("Registro de Clientes cancelado...\n");
		return -1;
	}
	GuardarRegistroCliente(&NuevoRegistroClientes,0);
	switch (PagarAnualidadConvenio(NuevoRegistroClientes.id,NuevoRegistroClientes.convenio)){
		case 1:
			break;
		case 0:
			printf("El convenio seleccionado no existe...\n");
			printf("Cancelando registro...\n");
			GuardarRegistroCliente(&NuevoRegistroClientes,0);
			return 0;
			break;
	}
	
	
	return 0;
}
int ModificarCliente(){//Esta función se encarga de obtener los datos de un Clientes y permitirle cambiarlos o no.
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	fclose(Archivo);
	struct clientes Temporal;
	int id=0,respuesta=1;
	printf("\t\tModulo de Control de clientes\n");
	printf("\t\t\tModificar cliente\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del cliente: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionCliente(id);//Recupera la información que tiene el Clientes
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El Clientes no existe\n");
		return -1;
	}
	while (respuesta){
		printf("\t\tModulo de Control de Clientes\n");
		printf("\t\t\tModificar cliente\n");
		printf("1.-Nombre completo del cliente: \"%s\"\n",Temporal.Nombre);
		printf("2.-Domicilio del cliente: \"%s\"\n",Temporal.Domicilio);
		printf("3.-Convenio del cliente: \"%i\"\n",Temporal.convenio);
		printf("4.-Pin del cliente: \"%i\"\n",Temporal.pin);
		printf("0.-Guardar y salir\n");
		printf("-1.-Salir sin guardar\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				printf("Ingresa el nuevo Nombre: ");
				getchar();
				scanf("%[^\n]",Temporal.Nombre);
				break;
			case 2:
				printf("Ingresa el nuevo domicilio del cliente: ");
				getchar();
				scanf("%[^\n]",Temporal.Domicilio);
				break;
			case 3:
				do{
					printf("Ingresa el nuevo convenio del cliente: ");
					scanf("%i",&Temporal.convenio);
				}while(!PagarAnualidadConvenio(Temporal.id,Temporal.convenio));
				break;
			case 4:
				printf("Ingresa el nuevo pin del cliente: ");
				scanf("%i",&Temporal.pin);
				break;
			case 0:
				GuardarRegistroCliente(&Temporal,Temporal.id);
				break;
			case -1:
				respuesta = 0;
				break;
		}
	}
	return 0;
}
int GuardarRegistroCliente(struct clientes *RegistroClientes, int idcliente){//Esta función sirve para guardar un registro de Clientes totalmente nuevo, modificar alguno o eliminarlo
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	struct clientes Temporal;
	int i=0;
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	if (!idcliente&&(*RegistroClientes).id)
	{
		fseek(Archivo,((*RegistroClientes).id-1)*sizeof(struct clientes),SEEK_SET);
		(*RegistroClientes).id=0;
		strcpy((*RegistroClientes).Nombre,"");
		strcpy((*RegistroClientes).Domicilio,"");
		(*RegistroClientes).convenio=0;
		(*RegistroClientes).pin=0;
		fwrite(RegistroClientes,sizeof(struct clientes),1,Archivo);
	}else if(!idcliente&&!(*RegistroClientes).id){
		while(!feof(Archivo)){
			++i;
			//printf("%i | %i |%i\n",i,Temporal.id ,idcliente);
			if (!Temporal.id)
			{
				(*RegistroClientes).id=i;
				fseek(Archivo,((*RegistroClientes).id-1)*sizeof(struct clientes),SEEK_SET);
				fwrite(RegistroClientes,sizeof(struct clientes),1,Archivo);
				fseek(Archivo,0,SEEK_END);
			}
			fread(&Temporal,sizeof(struct clientes),1,Archivo);
		}
		if (!(*RegistroClientes).id)
			printf("Ya no hay más espacio para Clientes...\n");
	}else if (idcliente&&(*RegistroClientes).id)
	{
		fseek(Archivo,((*RegistroClientes).id-1)*sizeof(struct clientes),SEEK_SET);
		fwrite(RegistroClientes,sizeof(struct clientes),1,Archivo);
	}
	fclose(Archivo);
	return 0;
}
struct clientes ObtenerInformacionCliente(int id){//Regresa la información de un cliente en base a su id.
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	struct clientes Temporal;
	fseek(Archivo,(id-1)*sizeof(struct clientes),SEEK_SET);
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	fclose(Archivo);
	return Temporal;
}
int EliminarCliente(){//Elimina a un cliente por medio de su id
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	fclose(Archivo);
	struct clientes Temporal;
	int id=0,respuesta;
	printf("\t\tModulo de Control de Clientes\n");
	printf("\t\t\tEliminar cliente\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del Clientes: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionCliente(id);//Recupera la información que tiene el Clientes
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El Clientes no existe\n");
		return -1;
	}
	printf("¿Realmente deseas eliminar al pobre \"%s\"? (1=Si|0=No)\n",Temporal.Nombre);
	printf("%i %s %s %i\n",Temporal.id,Temporal.Nombre,Temporal.Domicilio,Temporal.convenio);
	scanf("%i",&respuesta);
	if (respuesta)
		GuardarRegistroCliente(&Temporal,0);
	else
		printf("\"%s\" aun tiene empleo...\n",Temporal.Nombre);
	return 0;
}
int MostrarClientes(){//Muestra todos los clientes registrados
	FILE *Archivo=AbrirArchivo("Clientes.bin","rb+");
	struct clientes Temporal;
	fread(&Temporal,sizeof(struct clientes),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			printf("%i %s %s %i %i %f\n",Temporal.id,Temporal.Nombre,Temporal.Domicilio,Temporal.convenio,Temporal.pin,Temporal.TiempoAcumulado);
		}
		fread(&Temporal,sizeof(struct clientes),1,Archivo);
	}
	return 0;
}
int PagarAnualidadConvenio(int idcliente, int idconvenio){//Procesa el pago por la anualidad del convenio
	struct convenios convenio = ObtenerInformacionConvenio(idconvenio);
	if (!convenio.id)
		return 0;
	FILE * Archivo=AbrirArchivo("PagosConveniosHistorial.bin","rb+");
	int i=0;
	time_t raw_time;
	time(&raw_time);
	struct tm TiempoActual=*localtime(&raw_time);
	struct PagosConveniosHistorial Temporal;
	fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	while(!feof(Archivo)){
		++i;
		if (!Temporal.id)
		{
			Temporal.id=i;
			Temporal.idcliente=idcliente;
			Temporal.idconvenio=idconvenio;
			Temporal.PagoTotal=convenio.Anualidad;
			Temporal.FechaDePago=TiempoActual;
			printf("El pago por el convenio numero %i es %f\n",idconvenio,convenio.Anualidad);
			fseek(Archivo,(i-1)*sizeof(struct PagosConveniosHistorial),SEEK_SET);
			fwrite(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
			fclose(Archivo);
			return 1;
		}
		fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	}
	printf("El archivo \"PagosConveniosHistorial.bin\" ha llegado a su maxima capacidad\nHas un respaldo, elimina este y finalmente vuelve a iniciar el programa...\n");
	fclose(Archivo);
	exit(1);
}
int MostrarPagosConveniosHistorial(int Mostrar){
	FILE *Archivo=AbrirArchivo("PagosConveniosHistorial.bin","rb+");
	struct PagosConveniosHistorial Temporal;
	int contador=0;
	fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			contador ++;
			if (Mostrar)
				printf("%i %i %i %f %s",Temporal.id,Temporal.idcliente,Temporal.idconvenio,Temporal.PagoTotal,asctime(&Temporal.FechaDePago));
		}
		fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	}
	fclose(Archivo);
	return contador;
}
struct PagosConveniosHistorial ObtenerInformacionUltimoPagoConvenio(int idcliente){//Esta función recorrre el archivo "PagosConveniosHistorial.bin" de abajo hacia arriba para encontrar el pago de anualidad más reciente de un cliente
	FILE *Archivo=AbrirArchivo("PagosConveniosHistorial.bin","rb+");
	int TotalDeRegistros=0;
	struct PagosConveniosHistorial Temporal;
	fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			++TotalDeRegistros;
		}
		fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
	}
	for (int i = TotalDeRegistros; i > 0; i--)
	{
		fseek(Archivo,(i-1)*sizeof(struct PagosConveniosHistorial),SEEK_SET);
		fread(&Temporal,sizeof(struct PagosConveniosHistorial),1,Archivo);
		if (Temporal.idcliente==idcliente)
		{
			fclose(Archivo);
			return Temporal;
		}
	}
	printf("No hay ningun pago de Anualidad registrado con este cliente...\n");
	fclose(Archivo);
	Temporal.id=0;
	Temporal.idcliente=0;
	Temporal.idconvenio=0;
	Temporal.PagoTotal=0;
	struct tm TiempoCero={0,0,0,0,0,0,0,0,0};
	Temporal.FechaDePago=TiempoCero;
	return Temporal;
}
int ControlConvenios(){//Muestra el menu para control de los Convenios
	int respuesta=1;
	while(respuesta){
		printf("\t\tModulo de Control de Convenios\n");
		printf("1.-Agregar Convenio\n");
		printf("2.-Modificar Convenio\n");
		printf("3.-Eliminar Convenio\n");
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				AgregarConvenio();
				break;
			case 2:
				ModificarConvenio();
				break;
			case 3:
				EliminarConvenio();
				break;
			case 0:
				break;
			default:
				printf("Opción no encontrada...\n");
				break;
		}
	}
	return 0;
}
int AgregarConvenio(){//Esta funcion pide los datos necesarios para crear un nuevo Convenio y lo guarda
	FILE *Archivo=AbrirArchivo("Convenios.bin","rb+");
	fclose(Archivo);
	int minutos=0,horas=0,dias=0,semanas=0,meses=0, Periodo=1;
	struct convenios NuevoRegistroConvenios;
	NuevoRegistroConvenios.id=0;
	NuevoRegistroConvenios.LimiteDePeriodo=0;
	printf("\t\tModulo de Control de Convenios\n");
	printf("\t\t\tAgregar Convenios\n");
	printf("Ingresa \'0\' en todos los campos para cancelar\n");
	printf("Si deseas habilitar el pago a fin de periodo ingresa \'1\', de lo contrario \'0\': ");
	scanf("%i",&NuevoRegistroConvenios.PagoAFinDelPeriodo);
	printf("Ingresa la tasa de cobro por mes: ");
	scanf("%f",&NuevoRegistroConvenios.TasaPorMes);
	printf("Ingresa la tasa de cobro por semana: ");
	scanf("%f",&NuevoRegistroConvenios.TasaPorSemana);
	printf("Ingresa la tasa de cobro por dia: ");
	scanf("%f",&NuevoRegistroConvenios.TasaPorDia);
	printf("Ingresa la tasa de cobro por hora: ");
	scanf("%f",&NuevoRegistroConvenios.TasaPorHora);
	printf("Ingresa la tasa de cobro por minuto: ");
	scanf("%f",&NuevoRegistroConvenios.TasaPorMinuto);
	printf("Ingresa el costo de la anualidad: ");
	scanf("%f",&NuevoRegistroConvenios.Anualidad);
	if (NuevoRegistroConvenios.PagoAFinDelPeriodo){
		while(Periodo){
			printf("\t\tModulo de Control de Convenios\n");
			printf("\t\t\tAgregar Convenios\n");
			printf("Establece el tiempo maximo que se puede guardar en la cuenta del cliente...\n");
			printf("La cantidad actual es: %i meses %i semanas %i dias %i horas %i minutos\n",meses,semanas,dias,horas,minutos);
			printf("1.-Modificar meses\n");
			printf("2.-Modificar semanas\n");
			printf("3.-Modificar dias\n");
			printf("4.-Modificar horas\n");
			printf("5.-Modificar minutos\n");
			printf("0.-Guardar\n");
			scanf("%i",&Periodo);
			switch(Periodo){
				case 1:
					printf("Ingresa la nueva cantidad de meses: ");
					scanf("%i",&meses);
					break;
				case 2:
					printf("Ingresa la nueva cantidad de semanas: ");
					scanf("%i",&semanas);
					break;
				case 3:
					printf("Ingresa la nueva cantidad de dias: ");
					scanf("%i",&dias);
					break;
				case 4:
					printf("Ingresa la nueva cantidad de horas: ");
					scanf("%i",&horas);
					break;
				case 5:
					printf("Ingresa la nueva cantidad de minutos: ");
					scanf("%i",&minutos);
					break;
				case 0:
					NuevoRegistroConvenios.LimiteDePeriodo=minutos*60+horas*3600+dias*86400+semanas*604800+meses*1874800;
					break;
				default:
					printf("Opcion no encontrada...");
					break;
			}
		}
	}
	if (!NuevoRegistroConvenios.PagoAFinDelPeriodo&&!NuevoRegistroConvenios.TasaPorMes&&!NuevoRegistroConvenios.TasaPorSemana&&!NuevoRegistroConvenios.TasaPorDia&&!NuevoRegistroConvenios.TasaPorHora&&!NuevoRegistroConvenios.TasaPorMinuto&&!NuevoRegistroConvenios.Anualidad&&!NuevoRegistroConvenios.LimiteDePeriodo){
		printf("Registro de convenio cancelado...\n");
		return -1;
	}
	GuardarRegistroConvenio(&NuevoRegistroConvenios,0);
	return 0;
}
int ModificarConvenio(){//Esta función se encarga de obtener los datos de un Convenios y permitirle cambiarlos o no.
	FILE *Archivo=AbrirArchivo("Convenios.bin","rb+");
	fclose(Archivo);
	struct convenios Temporal;
	int id=0,respuesta=1,Periodo=1,	minutos=0,horas=0,dias=0,semanas=0,meses=0;
	char LimiteDePeriodo[100];
	printf("\t\tModulo de Control de Convenios\n");
	printf("\t\t\tModificar Convenio\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del Convenio: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionConvenio(id);//Recupera la información que tiene el Convenios
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El Convenios no existe\n");
		return -1;
	}
	while (respuesta){
		printf("\t\tModulo de Control de Convenios\n");
		printf("\t\t\tModificar Convenio\n");
		printf("1.-Pago a fin de periodo: %i\n",Temporal.PagoAFinDelPeriodo);
		printf("2.-La tasa de cobro por mes: %f\n",Temporal.TasaPorMes);
		printf("3.-La tasa de cobro por semana: %f\n",Temporal.TasaPorSemana);
		printf("4.-La tasa de cobro por dia: %f\n",Temporal.TasaPorDia);
		printf("5.-La tasa de cobro por hora: %f\n",Temporal.TasaPorHora);
		printf("6.-La tasa de cobro por minuto: %f\n",Temporal.TasaPorMinuto);
		printf("7.-El costo de la anualidad: %f\n",Temporal.Anualidad);
		ConvertirSegundos(Temporal.LimiteDePeriodo,LimiteDePeriodo);
		printf("8.-El periodo de tiempo: %s\n",LimiteDePeriodo);
		printf("0.-Guardar y salir\n");
		printf("-1.-Salir sin guardar\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				printf("Si deseas habilitar el pago a fin de periodo ingresa \'1\', de lo contrario \'0\': ");
				scanf("%i",&Temporal.PagoAFinDelPeriodo);
				break;
			case 2:
				printf("Ingresa la nueva tasa de cobro por mes: ");
				scanf("%f",&Temporal.TasaPorMes);
				break;
			case 3:
				printf("Ingresa la nueva tasa de cobro por semana: ");
				scanf("%f",&Temporal.TasaPorSemana);
				break;
			case 4:
				printf("Ingresa la nueva tasa de cobro por dia: ");
				scanf("%f",&Temporal.TasaPorDia);
				break;
			case 5:
				printf("Ingresa la nueva tasa de cobro por hora: ");
				scanf("%f",&Temporal.TasaPorHora);
				break;
			case 6:
				printf("Ingresa la nueva tasa de cobro por minuto: ");
				scanf("%f",&Temporal.TasaPorMinuto);
				break;
			case 7:
				printf("Ingresa el nuevo costo de la anualidad: ");
				scanf("%f",&Temporal.Anualidad);
				break;
			case 8:
				Periodo=1;
				if (Temporal.PagoAFinDelPeriodo){
					while(Periodo){
						printf("\t\tModulo de Control de Convenios\n");
						printf("\t\t\tModificar Convenios\n");
						printf("Establece el tiempo maximo que se puede guardar en la cuenta del cliente...\n");
						printf("La cantidad actual es: %i meses %i semanas %i dias %i horas %i minutos\n",meses,semanas,dias,horas,minutos);
						printf("1.-Modificar meses\n");
						printf("2.-Modificar semanas\n");
						printf("3.-Modificar dias\n");
						printf("4.-Modificar horas\n");
						printf("5.-Modificar minutos\n");
						printf("0.-Guardar\n");
						scanf("%i",&Periodo);
						switch(Periodo){
							case 1:
								printf("Ingresa la nueva cantidad de meses: ");
								scanf("%i",&meses);
								break;
							case 2:
								printf("Ingresa la nueva cantidad de semanas: ");
								scanf("%i",&semanas);
								break;
							case 3:
								printf("Ingresa la nueva cantidad de dias: ");
								scanf("%i",&dias);
								break;
							case 4:
								printf("Ingresa la nueva cantidad de horas: ");
								scanf("%i",&horas);
								break;
							case 5:
								printf("Ingresa la nueva cantidad de minutos: ");
								scanf("%i",&minutos);
								break;
							case 0:
								Temporal.LimiteDePeriodo=minutos*60+horas*3600+dias*86400+semanas*604800+meses*1874800;
								break;
							default:
								printf("Opcion no encontrada...");
								break;
							}
						}
					}
				break;
			case 0:
				GuardarRegistroConvenio(&Temporal,Temporal.id);
				break;
			case -1:
				respuesta = 0;
				break;
		}
	}
	return 0;
}
int GuardarRegistroConvenio(struct convenios *RegistroConvenios, int idConvenio){//Esta función sirve para guardar un registro de Convenios totalmente nuevo, modificar alguno o eliminarlo
	FILE *Archivo=AbrirArchivo("Convenios.bin","rb+");
	struct convenios Temporal;
	int i=0;
	fread(&Temporal,sizeof(struct convenios),1,Archivo);
	if (!idConvenio&&(*RegistroConvenios).id)
	{
		fseek(Archivo,((*RegistroConvenios).id-1)*sizeof(struct convenios),SEEK_SET);
		(*RegistroConvenios).id=0;
		(*RegistroConvenios).TasaPorMes=0;
		(*RegistroConvenios).TasaPorSemana=0;
		(*RegistroConvenios).TasaPorDia=0;
		(*RegistroConvenios).TasaPorHora=0;
		(*RegistroConvenios).TasaPorMinuto=0;
		(*RegistroConvenios).Anualidad=0;
		(*RegistroConvenios).LimiteDePeriodo=0;
		fwrite(RegistroConvenios,sizeof(struct convenios),1,Archivo);
	}else if(!idConvenio&&!(*RegistroConvenios).id){
		while(!feof(Archivo)){
			++i;
			//printf("%i | %i |%i\n",i,Temporal.id ,idConvenio);
			if (!Temporal.id)
			{
				(*RegistroConvenios).id=i;
				fseek(Archivo,((*RegistroConvenios).id-1)*sizeof(struct convenios),SEEK_SET);
				fwrite(RegistroConvenios,sizeof(struct convenios),1,Archivo);
				fseek(Archivo,0,SEEK_END);
			}
			fread(&Temporal,sizeof(struct convenios),1,Archivo);
		}
		if (!(*RegistroConvenios).id)
			printf("Ya no hay más espacio para Convenios...\n");
	}else if (idConvenio&&(*RegistroConvenios).id)
	{
		fseek(Archivo,((*RegistroConvenios).id-1)*sizeof(struct convenios),SEEK_SET);
		fwrite(RegistroConvenios,sizeof(struct convenios),1,Archivo);
	}
	fclose(Archivo);
	return 0;
}
struct convenios ObtenerInformacionConvenio(int id){//Regresa la información de un convenio en base a su id
	FILE * Archivo=AbrirArchivo("Convenios.bin","rb");
	struct convenios Temporal;
	fseek(Archivo,(id-1)*sizeof(struct convenios),SEEK_SET);
	fread(&Temporal,sizeof(struct convenios),1,Archivo);
	fclose(Archivo);
	return Temporal;
}
int EliminarConvenio(){//Elimina a un Convenio por medio de su id
	FILE *Archivo=AbrirArchivo("Convenios.bin","rb+");
	fclose(Archivo);
	struct convenios Temporal;
	int id=0,respuesta;
	printf("\t\tModulo de Control de Convenios\n");
	printf("\t\t\tEliminar Convenio\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Dame el id del Convenios: ");
	scanf("%i",&id);
	if (!id){
		printf("Saliendo...\n");
		return -1;
	}
	Temporal=ObtenerInformacionConvenio(id);//Recupera la información que tiene el Convenios
	if (!Temporal.id)//Comprueba que el realmente haya información en ese registro
	{
		printf("El Convenios no existe\n");
		return -1;
	}
	printf("¿Realmente deseas eliminar al pobre convenio con id=\"%i\"? (1=Si|0=No)\n",Temporal.id);
	printf("%i %f %f %f %f %f %f %f\n",Temporal.id,Temporal.TasaPorMes,Temporal.TasaPorSemana,Temporal.TasaPorDia,Temporal.TasaPorHora,Temporal.TasaPorMinuto,Temporal.Anualidad,Temporal.LimiteDePeriodo);
	scanf("%i",&respuesta);
	if (respuesta)
		GuardarRegistroConvenio(&Temporal,0);
	else
		printf("\"%i\" aun existe...\n",Temporal.id);
	return 0;
}
int MostrarConvenios(){//Muestra todos los Convenios registrados
	FILE *Archivo=AbrirArchivo("Convenios.bin","rb+");
	struct convenios Temporal;
	fread(&Temporal,sizeof(struct convenios),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			printf("%i %f %f %f %f %f %f %f\n",Temporal.id,Temporal.TasaPorMes,Temporal.TasaPorSemana,Temporal.TasaPorDia,Temporal.TasaPorHora,Temporal.TasaPorMinuto,Temporal.Anualidad,Temporal.LimiteDePeriodo);
		}
		fread(&Temporal,sizeof(struct convenios),1,Archivo);
	}
	return 0;
}
int ConvertirSegundos(float tiempo, char *Cadena){//Esta función recibe un numero de segundos y lo convierte meses,semanas,dias, o minutos. El resultado se guarda por referencia
	strcpy(Cadena,"");
	int minutos=0,horas=0,dias=0,semanas=0,meses=0,cociente=0;
	while(tiempo>0){
		if (tiempo>=2419200)
		{
			cociente=tiempo/2419200;
			tiempo-=cociente*2419200;
			meses++;
			continue;
		}
		if (tiempo>=604800)
		{
			cociente=tiempo/604800;
			tiempo-=cociente*604800;
			semanas++;
			continue;
		}
		if (tiempo>=86400)
		{
			cociente=tiempo/86400;
			tiempo-=cociente*86400;
			dias++;
			continue;
		}
		if (tiempo>=3600)
		{
			cociente=tiempo/3600;
			tiempo-=cociente*3600;
			horas++;
			continue;
		}
		if (tiempo>0)
		{
			minutos=tiempo/60;
			tiempo-=tiempo;

		}
	}
	sprintf(Cadena,"%i meses %i semanas %i dias %i horas %i minutos",meses,semanas,dias,horas,minutos);
	return 0;
}
int Reportes(){//Esta función le permite al administrador controlar los reportes.
	int respuesta=1;
	while(respuesta){
		printf("\t\tModulo de Reportes\n");
		printf("1.-Generar reporte\n");
		printf("2.-Abrir Reporte\n");
		printf("3.-Mostrar historial de reportes\n");
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				GenerarReporte();
				respuesta=0;
				break;
			case 2:
				AbrirReporte();
				respuesta=0;
				break;
			case 3:
				MostrarHistorialReportes();
				break;
			case 0:
				break;
			default:
				printf("Opcion no encontrada...\n");
				break;
		}
	}
	return 0;
}
int GenerarReporte(){//Esta función le permite al administrador generar los reportes mensuales, semanales o diarios segun como lo requiera.
	int respuesta=1;
	time_t raw_time;
	struct tm TiempoActual=*localtime(&raw_time);
	while(respuesta){
		time(&raw_time);
		TiempoActual=*localtime(&raw_time);
		int ElementosPagosEstacionamiento=MostrarPagosEstacionamientoHistorial(0),ElementosPagosConvenios=MostrarPagosConveniosHistorial(0);
		//printf("ElementosPagosEstacionamiento=%i\tMostrarPagosConveniosHistorial=%i\n",ElementosPagosEstacionamiento,ElementosPagosConvenios);
		printf("\t\tModulo de Reportes\n");
		printf("\t\tGenerar Reporte\n");
		if (ElementosPagosEstacionamiento)
		{
			printf("1.-Reporte de estacionamiento mensual\n");
			printf("2.-Reporte de estacionamiento semanal\n");
			printf("3.-Reporte de estacionamiento diario\n");
		}
		if (ElementosPagosConvenios)
		{
			printf("4.-Reporte de convenios mensual\n");
			printf("5.-Reporte de convenios semanal\n");
			printf("6.-Reporte de convenios diario\n");
		}
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				if (ElementosPagosEstacionamiento){
					respuesta=0;
					CrearReporteEstacionamiento(1,"ReporteEstacionamiento",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 2:
				if (ElementosPagosEstacionamiento){
					respuesta=0;
					CrearReporteEstacionamiento(2,"ReporteEstacionamiento",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 3:
				if (ElementosPagosEstacionamiento){
					respuesta=0;
					CrearReporteEstacionamiento(3,"ReporteEstacionamiento",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 4:
				if (ElementosPagosConvenios){
					respuesta=0;
					CrearReporteConvenios(1,"ReporteConvenios",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 5:
				if (ElementosPagosConvenios){
					respuesta=0;
					CrearReporteConvenios(2,"ReporteConvenios",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 6:
				if (ElementosPagosConvenios){
					respuesta=0;
					CrearReporteConvenios(3,"ReporteConvenios",TiempoActual);
				}
				else 
					printf("Opción no encontrada\n");
				break;
			case 0:
				break;
			default:
				printf("Opción no encontrada\n");
				break;
		}
	}
	return 0;
}
int AbrirReporte(){
	int id;
	printf("Ingresa el id del Reporte: ");
	scanf("%i",&id);
	if (!id)
	{
		printf("Saliendo...\n");
		return 0;
	}
	struct HistorialReportes Temporal=ObtenerInformacionReporte(id);
	if (!Temporal.id)
	{
		printf("El reporte no existe...\n");
		return 0;
	}
	if (DistinguirReportes(Temporal.Nombre))
	{
		//printf("Temporal_PagosEstacionamientoHistorial\n");
		FILE *Archivo=AbrirArchivo(Temporal.Nombre,"rb");
		float temp=Temporal.Periodo;
		char periodoletra[10];
		ObtenerValoresParaPeriodoReporte(&temp,periodoletra);
		struct PagosEstacionamientoHistorial Temporal_PagosEstacionamientoHistorial;
		printf("\tReporte de actividad: %s\n",Temporal.Nombre);
		printf("Id reporte: %i\tTipo: %s\tCreado: %s",Temporal.id,periodoletra,asctime(&Temporal.FechaDeCreacion));
		fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
		while(!feof(Archivo)){
			if (Temporal.id)
			{
				printf("%i %i %f %s",Temporal_PagosEstacionamientoHistorial.id,Temporal_PagosEstacionamientoHistorial.idcliente,Temporal_PagosEstacionamientoHistorial.PagoTotal,asctime(&Temporal_PagosEstacionamientoHistorial.FechaDePago));
			}
			fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo);
		}
		return 0;
	}else{
		//printf("Temporal_PagosConveniosHistorial\n");
		FILE *Archivo=AbrirArchivo(Temporal.Nombre,"rb");
		float temp=Temporal.Periodo;
		char periodoletra[10];
		ObtenerValoresParaPeriodoReporte(&temp,periodoletra);
		struct PagosConveniosHistorial Temporal_PagosConveniosHistorial;
		printf("\tReporte de actividad: %s\n",Temporal.Nombre);
		printf("Id reporte: %i\tTipo: %s\tCreado: %s",Temporal.id,periodoletra,asctime(&Temporal.FechaDeCreacion));
		fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosConveniosHistorial),1,Archivo);
		while(!feof(Archivo)){
			if (Temporal_PagosConveniosHistorial.id)
			{
				printf("%i %i %i %f %s",Temporal_PagosConveniosHistorial.id,Temporal_PagosConveniosHistorial.idcliente,Temporal_PagosConveniosHistorial.idconvenio,Temporal_PagosConveniosHistorial.PagoTotal,asctime(&Temporal_PagosConveniosHistorial.FechaDePago));
			}
			fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosConveniosHistorial),1,Archivo);
		}
		fclose(Archivo);
		return 0;
	}
	return 0;
}
int MostrarHistorialReportes(){
	FILE *Archivo=AbrirArchivo("HistorialReportes.bin","rb");
	struct HistorialReportes Temporal;
	fread(&Temporal,sizeof(struct HistorialReportes),1,Archivo);
	while(!feof(Archivo)){
		if (Temporal.id)
		{
			printf("%i %i %s %s\n",Temporal.id,Temporal.Periodo,Temporal.Nombre,asctime(&Temporal.FechaDeCreacion));
		}
		fread(&Temporal,sizeof(struct HistorialReportes),1,Archivo);
	}
	fclose(Archivo);
	return 0;
}
int CrearReporteEstacionamiento(int periodo,char *NombreDelReporte, struct tm TiempoActual){//Esta función crea un archivo de reporte con todas las actividades del estacionamiento
	int cont=0;
	float DifereciaDeTiempo=periodo;
	char NuevoNombre[100],periodoletra[100];
	char *FechaConLetra=asctime(&TiempoActual);
	QuitarEspacios(FechaConLetra);
	strcpy(NuevoNombre,NombreDelReporte);
	ObtenerValoresParaPeriodoReporte(&DifereciaDeTiempo, periodoletra);
	ComprobarArchivo(NombreDelReporte);
	struct PagosEstacionamientoHistorial Temporal_PagosEstacionamientoHistorial;
	struct HistorialReportes Temporal_HistorialReportes;
	FILE *Archivo_PagosEstacionamientoHistorial=AbrirArchivo("PagosEstacionamientoHistorial.bin","rb");
	FILE *Archivo_Reporte=AbrirArchivo(NombreDelReporte,"rb+");
	FILE *Archivo_ReporteHistorial=AbrirArchivo("HistorialReportes.bin","rb+");	
	fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosEstacionamientoHistorial);
	while(!feof(Archivo_PagosEstacionamientoHistorial)){
		if (difftime(mktime(&TiempoActual),mktime(&Temporal_PagosEstacionamientoHistorial.FechaDePago))<=DifereciaDeTiempo)//Esto checa que la diferencia de tiempo este dentro del periodo que se nos pidió
		{
			++cont;
			fwrite(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_Reporte);
		}
		fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosEstacionamientoHistorial);
	}
	if(cont){
		cont=0;
		fread(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		while(!feof(Archivo_ReporteHistorial)){//Este recorre el archivo de "HistorialReportes.bin"
			++cont;
			if (!Temporal_HistorialReportes.id)//Y encuentra un registro vacio para guardar el reporte
			{
				Temporal_HistorialReportes.id=cont;
				Temporal_HistorialReportes.Periodo=periodo;
				strcpy(Temporal_HistorialReportes.Nombre,NombreDelReporte);
				Temporal_HistorialReportes.FechaDeCreacion=TiempoActual;
				fseek(Archivo_ReporteHistorial,(Temporal_HistorialReportes.id-1)*sizeof(struct HistorialReportes),SEEK_SET);
				fwrite(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
				fseek(Archivo_ReporteHistorial,0,SEEK_END);
			}
			fread(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		}
		if (!Temporal_HistorialReportes.id){//Si no encontro ningun espacio vacio asume que ya se ha llenado todo
			printf("El archivo HistorialReportes.bin ha llegado a su maxima capacidad.\nHas un respaldo, elimina este y finalmente vuelve a iniciar el programa...\n");
		}
		fclose(Archivo_Reporte);
		sprintf(NuevoNombre,"%i_%s_%s_%s.bin",Temporal_HistorialReportes.id,NombreDelReporte,FechaConLetra,periodoletra);
		rename(NombreDelReporte,NuevoNombre);//Le pone un nombre que contenga el id del reporte, el tipo de reporte y 
		strcpy(Temporal_HistorialReportes.Nombre,NuevoNombre);
		fseek(Archivo_ReporteHistorial,(Temporal_HistorialReportes.id-1)*sizeof(struct HistorialReportes),SEEK_SET);
		fwrite(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		fclose(Archivo_ReporteHistorial);
		fclose(Archivo_PagosEstacionamientoHistorial);
		return 0;
	}
	printf("No se hallo ningun registro...\n");
	fclose(Archivo_Reporte);
	fclose(Archivo_ReporteHistorial);
	fclose(Archivo_PagosEstacionamientoHistorial);
	return 0;
}
int CrearReporteConvenios(int periodo,char *NombreDelReporte, struct tm TiempoActual){//Esta función crea un archivo de reporte con todas las actividades del estacionamiento
	int cont=0;
	float DifereciaDeTiempo=periodo;
	char NuevoNombre[100],periodoletra[100];
	char *FechaConLetra=asctime(&TiempoActual);
	QuitarEspacios(FechaConLetra);
	strcpy(NuevoNombre,NombreDelReporte);
	ObtenerValoresParaPeriodoReporte(&DifereciaDeTiempo, periodoletra);
	ComprobarArchivo(NombreDelReporte);
	struct PagosConveniosHistorial Temporal_PagosConveniosHistorial;
	struct HistorialReportes Temporal_HistorialReportes;
	FILE *Archivo_PagosConveniosHistorial=AbrirArchivo("PagosConveniosHistorial.bin","rb");
	FILE *Archivo_Reporte=AbrirArchivo(NombreDelReporte,"rb+");
	FILE *Archivo_ReporteHistorial=AbrirArchivo("HistorialReportes.bin","rb+");	
	fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosConveniosHistorial),1,Archivo_PagosConveniosHistorial);
	while(!feof(Archivo_PagosConveniosHistorial)){
		if (difftime(mktime(&TiempoActual),mktime(&Temporal_PagosConveniosHistorial.FechaDePago))<=DifereciaDeTiempo)//Esto checa que la diferencia de tiempo este dentro del periodo que se nos pidió
		{
			++cont;
			fwrite(&Temporal_PagosConveniosHistorial,sizeof(struct PagosConveniosHistorial),1,Archivo_Reporte);
		}
		fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosConveniosHistorial),1,Archivo_PagosConveniosHistorial);
	}
	if(cont){
		cont=0;
		fread(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		while(!feof(Archivo_ReporteHistorial)){//Este recorre el archivo de "HistorialReportes.bin"
			++cont;
			if (!Temporal_HistorialReportes.id)//Y encuentra un registro vacio para guardar el reporte
			{
				Temporal_HistorialReportes.id=cont;
				Temporal_HistorialReportes.Periodo=periodo;
				strcpy(Temporal_HistorialReportes.Nombre,NombreDelReporte);
				Temporal_HistorialReportes.FechaDeCreacion=TiempoActual;
				fseek(Archivo_ReporteHistorial,(Temporal_HistorialReportes.id-1)*sizeof(struct HistorialReportes),SEEK_SET);
				fwrite(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
				fseek(Archivo_ReporteHistorial,0,SEEK_END);
			}
			fread(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		}
		if (!Temporal_HistorialReportes.id){//Si no encontro ningun espacio vacio asume que ya se ha llenado todo
			printf("El archivo HistorialReportes.bin ha llegado a su maxima capacidad.\nHas un respaldo, elimina este y finalmente vuelve a iniciar el programa...\n");
		}
		fclose(Archivo_Reporte);
		sprintf(NuevoNombre,"%i_%s_%s_%s.bin",Temporal_HistorialReportes.id,NombreDelReporte,FechaConLetra,periodoletra);
		rename(NombreDelReporte,NuevoNombre);//Le pone un nombre que contenga el id del reporte, el tipo de reporte y 
		strcpy(Temporal_HistorialReportes.Nombre,NuevoNombre);
		fseek(Archivo_ReporteHistorial,(Temporal_HistorialReportes.id-1)*sizeof(struct HistorialReportes),SEEK_SET);
		fwrite(&Temporal_HistorialReportes,sizeof(struct HistorialReportes),1,Archivo_ReporteHistorial);
		fclose(Archivo_ReporteHistorial);
		fclose(Archivo_PagosConveniosHistorial);
		return 0;
	}
	printf("No se hallo ningun registro...\n");
	fclose(Archivo_Reporte);
	fclose(Archivo_ReporteHistorial);
	fclose(Archivo_PagosConveniosHistorial);
	return 0;
}
int ObtenerValoresParaPeriodoReporte(float *Periodo,char *periodoletra){//Obtiene los limites para el reporte de activiadad y el periodo en letra
	if (*Periodo==1	|| *Periodo==4){
		*Periodo=18748800;
		strcpy(periodoletra,"Mes");
	}if (*Periodo==2 || *Periodo==5){
		*Periodo=604800;
		strcpy(periodoletra,"Semana");
	}if (*Periodo==3 || *Periodo==6){
		*Periodo=86400;
		strcpy(periodoletra,"Dia");
	}
	return 0;
}
void QuitarEspacios(char *Cadena){//Quita los espacios de una cadena
	char Temporal[21]="";
	int cont=0,i;
	for (i = 0;Cadena[i]!='\0' ; ++i)
	{
		if (Cadena[i]!='\n'&&Cadena[i]!=' ')
		{
			if (Cadena[i]==':')
				Temporal[cont]='-';
			else
				Temporal[cont]=Cadena[i];
			cont++;
		}
	}
	Temporal[i]='\0';
	strcpy(Cadena,Temporal);
	return;
}
void QuitarSaltoDeLinea(char *Cadena){//Quita los saltos de linea de una cadena
	char Temporal[25]="";
	int cont=0,i;
	for (i = 0;Cadena[i]!='\0' ; ++i)
	{
		if (Cadena[i]!='\n')
		{
			Temporal[cont]=Cadena[i];
			cont++;
		}
	}
	Temporal[i]='\0';
	strcpy(Cadena,Temporal);
	return;
}
struct HistorialReportes ObtenerInformacionReporte(int id){
	FILE *Archivo=AbrirArchivo("HistorialReportes.bin","rb");
	struct HistorialReportes Temporal;
	fseek(Archivo,(id-1)*sizeof(struct HistorialReportes),SEEK_SET);
	fread(&Temporal,sizeof(struct HistorialReportes),1,Archivo);
	return Temporal;
}
int DistinguirReportes(char * Nombre){//Regresa el tipo de reporte que es de estacionamiento o de convenios.
	int i = 0;
	while(Nombre[i]!='R')
		i++;
	//printf("Nombre[i+7]=%c | i =%i\n",Nombre[9],i);
	if (Nombre[i+7]=='C')
	{
		return 0;
	}else if (Nombre[i+7]=='E')
	{
		return 1;
	}
	return -1;
}
int Facturacion(){//Genera la factura para los clientes
	int respuesta=1,id=0,pin;
	printf("\t\tFacturacion\n");
	printf("Ingresa \'0\' para salir...\n");
	printf("Ingresa el id del usuario: ");
	scanf("%i",&id);
	if (!id)
		return 0;
	struct clientes Temporal=ObtenerInformacionCliente(id);
	if (!Temporal.id){
		printf("El cliente no existe...\n");
		return 0;
	}
	if (id)
	{
		printf("Ingresa el pin: ");//Esto es para asegurar que es el cliente 
		scanf("%i",&pin);
		if(!ComprobarCliente(id, pin)){//Si el pin no coincide no permite hacer continuar con el registro
			printf("El pin ingresado es incorrecto\n");
			return 0;
		}
	}
	while(respuesta){
		printf("\t\tFacturacion\n");
		printf("1.-Factura mes\n");
		printf("2.-Factura semana\n");
		printf("3.-Factura dia\n");
		printf("0.-Salir\n");
		scanf("%i",&respuesta);
		switch(respuesta){
			case 1:
				GenerarFactura(Temporal.id,1);
				respuesta=0;
				break;
			case 2:
				GenerarFactura(Temporal.id,2);
				respuesta=0;
				break;
			case 3:
				GenerarFactura(Temporal.id,3);
				respuesta=0;
				break;
			case 0:
				break;
			default:
				printf("Caso no encontrado...n");
				break;
		}
	}
	return 0;	
}
int GenerarFactura(int id, int periodo){
	char NombreDeFactura[100],periodoletra[100];
	float DifereciaDeTiempo=periodo,PagoTotal=0;
	ObtenerValoresParaPeriodoReporte(&DifereciaDeTiempo,periodoletra);
	struct clientes Temporal_Cliente=ObtenerInformacionCliente(id);
	time_t raw_time;
	time(&raw_time);
	struct tm TiempoActual=*localtime(&raw_time);
	char *FechaConLetra=asctime(&TiempoActual);
	QuitarEspacios(FechaConLetra);
	sprintf(NombreDeFactura,"%i_%s_%s.txt",Temporal_Cliente.id,FechaConLetra,periodoletra);
	FILE * Archivo_FacturaTxt=AbrirArchivo(NombreDeFactura,"wt");
	fprintf(Archivo_FacturaTxt,"\tFactura\t");
    fprintf(Archivo_FacturaTxt,"Estacionamiento VIP SA de CV\tFecha:%s\nAguascalientes, AGS\nTEL:964-23-41\n",asctime(&TiempoActual));
    fprintf(Archivo_FacturaTxt,"***************************Pagos Estacionamiento***************************\n");
    FILE * Archivo_PagosEstacionamientoHistorial=AbrirArchivo("PagosEstacionamientoHistorial.bin","rb");
    FILE * Archivo_PagosConveniosHistorial=AbrirArchivo("PagosConveniosHistorial.bin","rb");
    struct PagosEstacionamientoHistorial Temporal_PagosEstacionamientoHistorial;
    struct PagosConveniosHistorial Temporal_PagosConveniosHistorial;
    fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosEstacionamientoHistorial);
    while(!feof(Archivo_PagosEstacionamientoHistorial)){
    	if (Temporal_PagosEstacionamientoHistorial.idcliente==Temporal_Cliente.id && difftime(mktime(&TiempoActual),mktime(&Temporal_PagosEstacionamientoHistorial.FechaDePago))<=DifereciaDeTiempo)
    	{
    		PagoTotal+=Temporal_PagosEstacionamientoHistorial.PagoTotal;
    		fprintf(Archivo_FacturaTxt,"%i %i %f %s\n",Temporal_PagosEstacionamientoHistorial.id,Temporal_PagosEstacionamientoHistorial.idcliente,Temporal_PagosEstacionamientoHistorial.PagoTotal,asctime(&Temporal_PagosEstacionamientoHistorial.FechaDePago));
    	}
    	fread(&Temporal_PagosEstacionamientoHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosEstacionamientoHistorial);
    }
    fprintf(Archivo_FacturaTxt,"***************************Pagos Convenios***************************\n");
    fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosEstacionamientoHistorial);
    while(!feof(Archivo_PagosConveniosHistorial)){
    	if (Temporal_PagosConveniosHistorial.idcliente==Temporal_Cliente.id && difftime(mktime(&TiempoActual),mktime(&Temporal_PagosConveniosHistorial.FechaDePago))<=DifereciaDeTiempo)
    	{
    		PagoTotal+=Temporal_PagosConveniosHistorial.PagoTotal;
    		fprintf(Archivo_FacturaTxt,"%i %i %i %f %s\n",Temporal_PagosConveniosHistorial.id,Temporal_PagosConveniosHistorial.idcliente,Temporal_PagosConveniosHistorial.idconvenio,Temporal_PagosConveniosHistorial.PagoTotal,asctime(&Temporal_PagosConveniosHistorial.FechaDePago));
    	}
    	fread(&Temporal_PagosConveniosHistorial,sizeof(struct PagosEstacionamientoHistorial),1,Archivo_PagosConveniosHistorial);
    }
    fprintf(Archivo_FacturaTxt,"Pago Total------------------------------------------------------>%f\n",PagoTotal);
    fclose(Archivo_PagosConveniosHistorial);
    fclose(Archivo_PagosEstacionamientoHistorial);
    fclose(Archivo_FacturaTxt);
    return 0;
}